<p align="center"><img src="https://i.imgur.com/a9QWW0v.png"></p>

### Use it

```
# development mode
$ yarn dev (or `npm run dev` or `pnpm run dev`)

# production build
$ yarn build (or `npm run build` or `pnpm run build`)
```
