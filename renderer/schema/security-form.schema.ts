import * as Yup from 'yup';

export const securityFormSchema = Yup.object({
  currentPassword: Yup.string().required('Current Password is a required field!'),
  newPassword: Yup.string().required('New Password is a required field!'),
  confirmPass: Yup.string().oneOf([Yup.ref('newPassword')], 'Passwords must match'),
});
