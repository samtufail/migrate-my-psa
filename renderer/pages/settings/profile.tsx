import React from 'react';
import { ProfileForm } from 'containers';
import { Layout } from 'components';
export default function ProfileSettings() {
  return (
    <Layout>
      <ProfileForm />
    </Layout>
  );
}
