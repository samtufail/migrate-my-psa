import React from 'react';
import { SecurityForm } from 'containers';
import { Layout } from 'components';
export default function SecuritySettings() {
  return (
    <Layout>
      <SecurityForm />
    </Layout>
  );
}
