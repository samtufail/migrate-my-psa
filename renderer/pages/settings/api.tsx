import React from 'react';
import { APIForm } from 'containers';
import { Layout } from 'components';
export default function APIKeysSettings() {
  return (
    <Layout>
      <APIForm />
    </Layout>
  );
}
