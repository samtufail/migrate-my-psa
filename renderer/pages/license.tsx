import React from 'react';
import { LicenseForm } from 'containers';
import { Layout } from 'components';
export default function LicensePage() {
  return (
    <Layout>
      <LicenseForm />
    </Layout>
  );
}
