import React from 'react';
import { Migrations } from 'containers';
import { Layout } from 'components';
export default function Dashboard() {
  return (
    <Layout>
      <Migrations />
    </Layout>
  );
}
