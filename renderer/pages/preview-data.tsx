import React from 'react';
import { PreviewData } from 'containers';
import { Layout } from 'components';
export default function LicensePage() {
  return (
    <Layout>
      <PreviewData />
    </Layout>
  );
}
