import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import type { AppProps } from 'next/app';
import { setUser, useAppDispatch, useAppSelector, wrapper } from 'store';
import { ToastContainer } from 'react-toastify';
import { onAuthStateChanged } from 'firebase/auth';
import { auth } from 'utils/firebase';

import '../styles/globals.css';
import 'react-toastify/dist/ReactToastify.css';
import { useValidateMutation } from 'store/license';
import { LoadingOutlined } from '@ant-design/icons';

function MyApp({ Component, pageProps }: AppProps) {
  const [validate, { isLoading, error, isError }] = useValidateMutation();
  const [loading, setLoading] = useState(false);
  const user = useAppSelector((state) => state?.user);
  const dispatch = useAppDispatch();
  // check at page load if a user is authenticated
  useEffect(() => {
    onAuthStateChanged(auth, (userAuth) => {
      if (userAuth && !user?.email) {
        // user is logged in, send the user's details to redux, store the current user in the state
        let images: any;
        let apiKeys: any;
        if (typeof window !== 'undefined') {
          images = JSON.parse(localStorage.getItem('userImage')) || {};
          apiKeys = JSON.parse(localStorage.getItem('apiKeys'));
        }
        dispatch(
          setUser({
            email: userAuth.email,
            uid: userAuth.uid,
            displayName: userAuth.displayName,
            photoUrl: images?.[userAuth?.uid] || '/images/user.png',
            ['syncro-apikey']: apiKeys?.[userAuth?.uid]?.['syncro-apikey'],
            ['syncro-subdomain']: apiKeys?.[userAuth?.uid]?.['syncro-subdomain'],
            ['superops-apikey']: apiKeys?.[userAuth?.uid]?.['superops-apikey'],
            ['superops-subdomain']: apiKeys?.[userAuth?.uid]?.['superops-subdomain'],
          })
        );
      }
    });

    (async () => {
      setLoading(true);
      try {
        if (typeof window !== 'undefined') {
          const key = localStorage.getItem('key');
          if (key) {
            await validate({ key: key }).unwrap();
          }
        }
      } catch (e) {
        console.log(e);
      } finally {
        setLoading(false);
      }
    })();
  }, []);

  return (
    <>
      <Head>
        <title>Migrate My PSA</title>
      </Head>
      <ToastContainer
        position="top-right"
        autoClose={2500}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
      />
      {loading ? (
        <div className="w-screen h-screen flex items-center justify-center">
          <LoadingOutlined className="text-[70px]" />
        </div>
      ) : (
        <Component {...pageProps} />
      )}
    </>
  );
}

export default wrapper.withRedux(MyApp);
