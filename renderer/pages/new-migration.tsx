import React from 'react';
import { MigrationForm } from 'containers';
import { Layout } from 'components';
export default function AddNewMigration() {
  return (
    <Layout>
      <MigrationForm />
    </Layout>
  );
}
