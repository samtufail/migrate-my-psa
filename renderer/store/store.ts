import { Store, combineReducers, configureStore } from '@reduxjs/toolkit';
import userReducer from './user/user.slice';
import licenseReducer from './license/license.slice';
import userApiSlice from './user/user.api';
import { TypedUseSelectorHook, useDispatch, useSelector, useStore } from 'react-redux';
import { createWrapper } from 'next-redux-wrapper';
import licenseApiSlice from './license/license.api';

export const makeStore = () =>
  configureStore({
    reducer: combineReducers({
      user: userReducer,
      license: licenseReducer,
      [userApiSlice.reducerPath]: userApiSlice.reducer,
      [licenseApiSlice.reducerPath]: licenseApiSlice.reducer,
    }),
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware({
        serializableCheck: false,
      })
        .concat(userApiSlice.middleware)
        .concat(licenseApiSlice.middleware),
    devTools: process.env.NODE_ENV !== 'production',
  });

export const store = makeStore();

export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
export const useAppDispatch: () => AppDispatch = useDispatch;
export const useAppStore: () => AppStore = useStore;
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export type AppStore = ReturnType<typeof makeStore>;

export const wrapper = createWrapper<Store<RootState>>(makeStore);
