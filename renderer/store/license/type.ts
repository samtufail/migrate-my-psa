export interface LicenseRes {
  data: Meta2;
}

export interface Data {
  id: string;
  type: string;
  attributes: Attributes;
  relationships: Relationships;
  links: Links10;
}

export interface Attributes {
  name: any;
  key: string;
  expiry: string;
  status: string;
  uses: number;
  suspended: boolean;
  scheme: any;
  encrypted: boolean;
  strict: boolean;
  floating: boolean;
  protected: boolean;
  version: any;
  maxMachines: number;
  maxProcesses: any;
  maxCores: any;
  maxUses: any;
  requireHeartbeat: boolean;
  requireCheckIn: boolean;
  lastValidated: string;
  lastCheckIn: any;
  nextCheckIn: any;
  lastCheckOut: any;
  metadata: Metadata;
  created: string;
  updated: string;
}

export interface Metadata {}

export interface Relationships {
  account: Account;
  environment: Environment;
  product: Product;
  policy: Policy;
  group: Group;
  user: User;
  machines: Machines;
  tokens: Tokens;
  entitlements: Entitlements;
}

export interface Account {
  links: Links;
  data: Data2;
}

export interface Links {
  related: string;
}

export interface Data2 {
  type: string;
  id: string;
}

export interface Environment {
  links: Links2;
  data: any;
}

export interface Links2 {
  related: any;
}

export interface Product {
  links: Links3;
  data: Data3;
}

export interface Links3 {
  related: string;
}

export interface Data3 {
  type: string;
  id: string;
}

export interface Policy {
  links: Links4;
  data: Data4;
}

export interface Links4 {
  related: string;
}

export interface Data4 {
  type: string;
  id: string;
}

export interface Group {
  links: Links5;
  data: any;
}

export interface Links5 {
  related: string;
}

export interface User {
  links: Links6;
  data: any;
}

export interface Links6 {
  related: string;
}

export interface Machines {
  links: Links7;
  meta: Meta;
}

export interface Links7 {
  related: string;
}

export interface Meta {
  cores: number;
  count: number;
}

export interface Tokens {
  links: Links8;
}

export interface Links8 {
  related: string;
}

export interface Entitlements {
  links: Links9;
}

export interface Links9 {
  related: string;
}

export interface Links10 {
  self: string;
}

export interface Meta2 {
  ts: string;
  valid: boolean;
  detail: string;
  code: string;
}
