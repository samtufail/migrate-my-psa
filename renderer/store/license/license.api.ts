import { createApi, fakeBaseQuery } from '@reduxjs/toolkit/query/react';
import axios from 'axios';

interface LicensePayload {
  key: string;
}

const licenseApiSlice = createApi({
  reducerPath: 'licenseApi',
  baseQuery: fakeBaseQuery(),
  endpoints: (builder) => ({
    // Validate License
    validate: builder.mutation<any, LicensePayload>({
      queryFn: async ({ key }) => {
        let data = { meta: { key: '' } };
        if (typeof window !== 'undefined') {
          const currentKey = localStorage.getItem('key');
          if (currentKey) {
            data.meta.key = currentKey;
          } else {
            data.meta.key = key;
            localStorage.setItem('key', key);
          }
        }
        try {
          const res = await axios.post(
            `https://api.keygen.sh/v1/accounts/${process.env.NEXT_PUBLIC_ACCOUNT_ID}/licenses/actions/validate-key`,
            data,
            { headers: { Authorization: `Bearer ${process.env.NEXT_PUBLIC_TOKEN}` } }
          );
          return { data: res?.data?.meta };
        } catch (e) {
          console.log(e);
        }
      },
    }),
  }),
});

export const { useValidateMutation } = licenseApiSlice;

export default licenseApiSlice;
