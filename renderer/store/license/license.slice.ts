import { createSlice } from '@reduxjs/toolkit';
import { RootState } from '..';
import licenseApiSlice from './license.api';

// Define the initial state using that type
const initialState = {
  status: false,
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {},
  extraReducers(builder) {
    builder.addMatcher(licenseApiSlice.endpoints.validate.matchFulfilled, (state, { payload }) => {
      console.log(payload);
      state.status = payload?.valid;
    });
  },
});

// selectors
export const selectStatus = (state: RootState) => state?.license?.status;

export default userSlice.reducer;
