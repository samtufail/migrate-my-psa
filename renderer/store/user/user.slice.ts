import { createSlice } from '@reduxjs/toolkit';
import { RootState } from '..';
import userApiSlice from './user.api';

// Define the initial state using that type
const initialState = {
  email: '',
  uid: '',
  displayName: '',
  photoUrl: '',
  ['syncro-apikey']: '',
  ['syncro-subdomain']: '',
  ['superops-apikey']: '',
  ['superops-subdomain']: '',
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    logout: (state) => {
      state.email = '';
      state.uid = '';
      state.displayName = '';
      state.photoUrl = '';
    },
    setUser: (state, action) => {
      state.email = action?.payload?.email;
      state.uid = action?.payload?.uid;
      state.displayName = action?.payload?.displayName;
      state.photoUrl = action?.payload?.photoUrl;
      state['syncro-apikey'] = action?.payload?.['syncro-apikey'];
      state['syncro-subdomain'] = action?.payload?.['syncro-subdomain'];
      state['superops-apikey'] = action?.payload?.['superops-apikey'];
      state['superops-subdomain'] = action?.payload?.['superops-subdomain'];
    },
  },
  extraReducers(builder) {
    builder.addMatcher(userApiSlice.endpoints.signIn.matchFulfilled, (state, { payload }) => {
      state.email = payload.email;
      state.uid = payload.uid;
      state.displayName = payload.displayName;
      state.photoUrl = payload.photoUrl;
      state['syncro-apikey'] = payload?.['syncro-apikey'];
      state['syncro-subdomain'] = payload?.['syncro-subdomain'];
      state['superops-apikey'] = payload?.['superops-apikey'];
      state['superops-subdomain'] = payload?.['superops-subdomain'];
    });
    builder.addMatcher(userApiSlice.endpoints.signUp.matchFulfilled, (state, { payload }) => {
      state.email = payload.email;
      state.uid = payload.uid;
      state.displayName = payload.displayName;
      state.photoUrl = payload.photoUrl;
      state['syncro-apikey'] = payload?.['syncro-apikey'];
      state['syncro-subdomain'] = payload?.['syncro-subdomain'];
      state['superops-apikey'] = payload?.['superops-apikey'];
      state['superops-subdomain'] = payload?.['superops-subdomain'];
    });
    builder.addMatcher(userApiSlice.endpoints.updateUser.matchFulfilled, (state, { payload }) => {
      state.email = payload.email;
      state.uid = payload.uid;
      state.displayName = payload.displayName;
      state.photoUrl = payload.photoUrl;
      state['syncro-apikey'] = payload?.['syncro-apikey'];
      state['syncro-subdomain'] = payload?.['syncro-subdomain'];
      state['superops-apikey'] = payload?.['superops-apikey'];
      state['superops-subdomain'] = payload?.['superops-subdomain'];
    });
    builder.addMatcher(userApiSlice.endpoints.updateAPIKeys.matchFulfilled, (state, { payload }) => {
      state.email = payload.email;
      state.uid = payload.uid;
      state.displayName = payload.displayName;
      state.photoUrl = payload.photoUrl;
      state['syncro-apikey'] = payload?.['syncro-apikey'];
      state['syncro-subdomain'] = payload?.['syncro-subdomain'];
      state['superops-apikey'] = payload?.['superops-apikey'];
      state['superops-subdomain'] = payload?.['superops-subdomain'];
    });
  },
});

export const { logout, setUser } = userSlice.actions;

// selectors
export const selectUser = (state: RootState) => state?.user;

export default userSlice.reducer;
