import { createApi, fakeBaseQuery } from '@reduxjs/toolkit/query/react';
import {
  createUserWithEmailAndPassword,
  sendPasswordResetEmail,
  signInWithEmailAndPassword,
  signOut,
  updatePassword,
  updateProfile,
} from 'firebase/auth';
import { auth } from 'utils/firebase';

export interface SignInPayload {
  email: string;
  password: string;
}

export interface SignInRes {
  email: string;
  uid: string;
  displayName: string;
  photoUrl: string;
}

export interface APIKeys {
  ['syncro-apikey']: string;
  ['syncro-subdomain']: string;
  ['superops-apikey']: string;
  ['superops-subdomain']: string;
}

const userApiSlice = createApi({
  reducerPath: 'userApi',
  baseQuery: fakeBaseQuery(),
  endpoints: (builder) => ({
    // Signup
    signUp: builder.mutation<SignInRes & APIKeys, SignInPayload>({
      queryFn: async ({ email, password }) => {
        return createUserWithEmailAndPassword(auth, email, password)
          .then(({ user }) => {
            let images: any;
            let apiKeys: any;
            if (typeof window !== 'undefined') {
              images = JSON.parse(localStorage.getItem('userImage'));
              apiKeys = JSON.parse(localStorage.getItem('apiKeys'));
            }
            return {
              data: {
                email: user.email,
                uid: user.uid,
                displayName: user.displayName,
                photoUrl: images?.[auth.currentUser.uid] || '/images/user.png',
                ['syncro-apikey']: apiKeys?.[auth?.currentUser?.uid]?.['syncro-apikey'],
                ['syncro-subdomain']: apiKeys?.[auth?.currentUser?.uid]?.['syncro-subdomain'],
                ['superops-apikey']: apiKeys?.[auth?.currentUser?.uid]?.['superops-apikey'],
                ['superops-subdomain']: apiKeys?.[auth?.currentUser?.uid]?.['superops-subdomain'],
              },
            };
          })
          .catch((error) => {
            return { error: { ...error } };
          });
      },
    }),
    // Signin
    signIn: builder.mutation<SignInRes, SignInPayload>({
      queryFn: async ({ email, password }) => {
        return signInWithEmailAndPassword(auth, email, password)
          .then(({ user }) => {
            let images: any;
            let apiKeys: any;
            if (typeof window !== 'undefined') {
              images = JSON.parse(localStorage.getItem('userImage'));
              apiKeys = JSON.parse(localStorage.getItem('apiKeys'));
            }
            return {
              data: {
                email: user.email,
                uid: user.uid,
                displayName: user.displayName,
                photoUrl: images?.[auth.currentUser.uid] || '/images/user.png',
                ['syncro-apikey']: apiKeys?.[auth?.currentUser?.uid]?.['syncro-apikey'],
                ['syncro-subdomain']: apiKeys?.[auth?.currentUser?.uid]?.['syncro-subdomain'],
                ['superops-apikey']: apiKeys?.[auth?.currentUser?.uid]?.['superops-apikey'],
                ['superops-subdomain']: apiKeys?.[auth?.currentUser?.uid]?.['superops-subdomain'],
              },
            };
          })
          .catch((error) => {
            return { error: { ...error } };
          });
      },
    }),
    // Signout
    signout: builder.mutation<{ message: string }, void>({
      queryFn: async () => {
        await signOut(auth);
        return { data: { message: 'Logged Out' } };
      },
    }),
    // Forgot Password
    forgotPassword: builder.mutation<{ message: string }, { email: string }>({
      queryFn: async ({ email }) => {
        await sendPasswordResetEmail(auth, email);
        return { data: { message: 'Email Sent' } };
      },
    }),
    // Update User
    updateUser: builder.mutation<SignInRes & APIKeys, { name: string; image: string }>({
      queryFn: async ({ name, image }) => {
        if (image) {
          if (typeof window !== 'undefined') {
            const currentImages = JSON.parse(localStorage.getItem('userImages')) || {};
            const images = { ...currentImages, [auth.currentUser.uid]: image };
            localStorage.setItem('userImage', JSON.stringify(images));
          }
        }
        if (name) {
          await updateProfile(auth.currentUser, { displayName: name });
        }
        let newImages: any;
        let apiKeys: any;
        if (typeof window !== 'undefined') {
          newImages = JSON.parse(localStorage.getItem('userImage'));
          apiKeys = JSON.parse(localStorage.getItem('apiKeys'));
        }
        return {
          data: {
            email: auth.currentUser.email,
            uid: auth.currentUser.uid,
            displayName: auth.currentUser.displayName,
            photoUrl: newImages?.[auth.currentUser.uid] || '/images/user.png',
            ['syncro-apikey']: apiKeys?.[auth?.currentUser?.uid]?.['syncro-apikey'],
            ['syncro-subdomain']: apiKeys?.[auth?.currentUser?.uid]?.['syncro-subdomain'],
            ['superops-apikey']: apiKeys?.[auth?.currentUser?.uid]?.['superops-apikey'],
            ['superops-subdomain']: apiKeys?.[auth?.currentUser?.uid]?.['superops-subdomain'],
          },
        };
      },
    }),
    // Update API Keys
    updateAPIKeys: builder.mutation<SignInRes & APIKeys, APIKeys>({
      queryFn: async (values) => {
        if (values) {
          if (typeof window !== 'undefined') {
            const currentAPIKeys = JSON.parse(localStorage.getItem('apiKeys')) || {};
            const apiKeys = {
              ...currentAPIKeys,
              [auth.currentUser.uid]: {
                ['syncro-apikey']: values['syncro-apikey'],
                ['syncro-subdomain']: values['syncro-subdomain'],
                ['superops-apikey']: values['superops-apikey'],
                ['superops-subdomain']: values['superops-subdomain'],
              },
            };
            localStorage.setItem('apiKeys', JSON.stringify(apiKeys));
          }
          const newImages = JSON.parse(localStorage.getItem('userImage'));
          return {
            data: {
              email: auth.currentUser.email,
              uid: auth.currentUser.uid,
              displayName: auth.currentUser.displayName,
              photoUrl: newImages?.[auth.currentUser.uid] || '/images/user.png',
              ['syncro-apikey']: values['syncro-apikey'],
              ['syncro-subdomain']: values['syncro-subdomain'],
              ['superops-apikey']: values['superops-apikey'],
              ['superops-subdomain']: values['superops-subdomain'],
            },
          };
        }
      },
    }),
    // Update Password
    updatePassword: builder.mutation<{ message: string }, { currentPassword: string; newPassword: string }>({
      queryFn: async ({ currentPassword, newPassword }) => {
        try {
          const userCred = await signInWithEmailAndPassword(auth, auth.currentUser.email, currentPassword);
          if (userCred?.user) {
            await updatePassword(userCred.user, newPassword);
            return { data: { message: 'Updated!' } };
          }
        } catch (err) {
          return { error: { ...err } };
        }
      },
    }),
  }),
});

export const {
  useSignInMutation,
  useSignUpMutation,
  useSignoutMutation,
  useForgotPasswordMutation,
  useUpdateUserMutation,
  useUpdatePasswordMutation,
  useUpdateAPIKeysMutation,
} = userApiSlice;

export default userApiSlice;
