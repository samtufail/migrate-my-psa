import { LoadingOutlined } from '@ant-design/icons';
import { ArrowLeftCircleIcon } from '@heroicons/react/24/outline';
import { DButton, Input } from 'components';
import { Form, Formik } from 'formik';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { toast } from 'react-toastify';
import { securityFormSchema } from 'schema';
import { useUpdatePasswordMutation } from 'store';
import { getFirebaseErrorMessage } from 'utils';

export const SecurityForm = () => {
  const router = useRouter();

  const [updatePassword, { isLoading }] = useUpdatePasswordMutation();

  return (
    <>
      <div className="max-w-5xl mx-auto mt-7">
        <div className="flex justify-between items-center">
          <DButton type="button" className="w-[90px]" onClick={() => router.back()}>
            <ArrowLeftCircleIcon className="block h-6 w-6" aria-hidden="true" />
            <p>Back</p>
          </DButton>
          <h2 className="title text-secondary">Settings</h2>
        </div>

        <div className="shadow-md sm:rounded-lg my-4 p-[22px] font-source-sans-pro flex gap-[20px] w-full">
          <div className="md:w-[20%] flex flex-col">
            <Link href="/settings/profile">
              <DButton type="button" className="w-[90px] mb-2">
                Profile
              </DButton>
            </Link>
            <Link href="/settings/api">
              <DButton type="button" className="w-[90px] mb-2">
                API
              </DButton>
            </Link>
            <Link href="/settings/security">
              <DButton type="button" className="w-[90px] bg-secondary">
                Security
              </DButton>
            </Link>
          </div>
          <div className="md:w-[60%]">
            <h2 className="title text-secondary">Security</h2>
            <Formik
              initialValues={{ currentPassword: '', newPassword: '', confirmPass: '' }}
              validationSchema={securityFormSchema}
              onSubmit={async ({ currentPassword, confirmPass }, { resetForm }) => {
                updatePassword({ currentPassword, newPassword: confirmPass })
                  .unwrap()
                  .then((res) => {
                    toast.success('Password Updated Successfully!', {
                      position: 'top-right',
                      autoClose: 2500,
                      hideProgressBar: false,
                      closeOnClick: true,
                      pauseOnHover: true,
                      draggable: true,
                      progress: undefined,
                      theme: 'light',
                    })
                    resetForm();
                  }
                  )
                  .catch((err) =>
                    toast.error(getFirebaseErrorMessage(err?.code), {
                      position: 'top-right',
                      autoClose: 2500,
                      hideProgressBar: false,
                      closeOnClick: true,
                      pauseOnHover: true,
                      draggable: true,
                      progress: undefined,
                      theme: 'light',
                    })
                  );
              }}
            >
              {() => {
                return (
                  <Form className="flex flex-col gap-[25px] w-full max-w-[420px]">
                    <Input name="currentPassword" type="password" label="Current Password" placeholder="••••••••••••" />
                    <Input name="newPassword" type="password" label="New Password" placeholder="••••••••••••" />
                    <Input name="confirmPass" type="password" label="Confirm Password" placeholder="••••••••••••" />
                    <div className="flex items-center gap-2 justify-end">
                      <DButton type="submit" className="bg-secondary w-[150px] justify-center" disabled={isLoading}>
                        {isLoading ? (
                          <>
                            <LoadingOutlined />
                          </>
                        ) : (
                          'Change Password'
                        )}
                      </DButton>
                    </div>
                  </Form>
                );
              }}
            </Formik>
          </div>
        </div>
      </div>
    </>
  );
};
