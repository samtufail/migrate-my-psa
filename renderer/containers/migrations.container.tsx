import { DButton } from 'components';
import { PlusCircleIcon } from '@heroicons/react/24/outline';
import { MigrationContent } from './migration-content.container';
import Link from 'next/link';
import { LockOutlined } from '@ant-design/icons';
import { useAppSelector } from 'store';
import { selectStatus } from 'store/license';

export function Migrations() {
  const status = useAppSelector(state => state.license.status);
  return (
    <div className="relative w-full h-full">
      {!status ? <div className="w-5xl bg-[#2ec4b6] p-3 rounded-lg z-50 left-[50%] top-[50%] absolute transform translate-x-[-50%] translate-y-[-50%] flex flex-col items-center justify-center">
        <h2 className='mb-2 text-[20px]'> <LockOutlined />&nbsp;Please active product before proceeding further.</h2>
        <Link href="/license">
          <DButton className="justify-center">Activate</DButton>
        </Link>
      </div> : <></>}
      <div className={status ? "max-w-5xl mx-auto mt-7 filter" : "max-w-5xl mx-auto mt-7 filter blur-sm"}>
        <div className="flex md:flex-row flex-col justify-between items-center ">
          <h2 className="sub-title text-black text-[30px]">Migrations</h2>
          <Link href="/new-migration">
            <DButton type="button">
              <PlusCircleIcon className="block h-6 w-6" aria-hidden="true" />
              <p>New Migration</p>
            </DButton>
          </Link>
        </div>

        {/* Table  */}
        <div className="relative  shadow-md sm:rounded-lg my-4 p-[22px] font-source-sans-pro">
          <table className="w-full text-left">
            <thead className="text-[12px] text-[#6B7280] bg-[#F9FAFB] uppercase">
              <tr>
                <th scope="col" className="px-6 py-3">
                  UID
                </th>
                <th scope="col" className="px-6 py-3">
                  Migration type
                </th>
                <th scope="col" className="px-6 py-3">
                  start date
                </th>
                <th scope="col" className="px-6 py-3">
                  end date
                </th>
                <th scope="col" className="px-6 py-3">
                  Status
                </th>
              </tr>
            </thead>
            <MigrationContent />
          </table>
        </div>
      </div>
    </div>
  );
}
