import { Formik, Form } from 'formik';
import { Input, Button } from '../components';

export function Reset() {
  return (
    <>
      <div className=" text-[#64748B] grid md:grid-cols-2 grid-cols-1 min-h-screen">
        {/* Logo */}
        <div className="flex flex-col gap-[36px] items-center justify-center">
          <div className="px-[12px]">
            <h1 className="md:text-[33px] text-[22px] text-center uppercase text-secondary font-norwester">
              Migrate <span className="text-primary">My PSA</span>
            </h1>
          </div>
          <p className="hidden md:flex text-[16px] font-[400]  max-w-[318px] text-center">
            All in one solution to migrate data between SyncroMSP and SuperOps.Ai
          </p>
          <div>
            <img src="/images/left-image.png" alt="left-image" className="hidden md:flex max-w-[349px]" />
          </div>
        </div>

        <div className="flex flex-col gap-[36px] items-center justify-center">
          <h1 className="md:text-[33px] text-[22px] text-center uppercase text-secondary font-norwester">Reset Password</h1>

          <Formik
            initialValues={{ password: '', confirmPass: '' }}
            // Add your validation schema here if needed
            onSubmit={(values) => {
              console.log(values);
            }}
          >
            {({ errors, touched }) => (
              <Form className=" flex flex-col gap-[30px] w-full max-w-[428px] p-[12px]">
                {/* Input fields */}
                <Input name="password" type="password" label="Password" placeholder="••••••••••••" />
                <Input name="confirmPass" type="password" label="Confirm Password" placeholder="••••••••••••" />

                {/* button */}
                <Button type="submit" className="flex justify-center items-center">
                  Reset Password
                </Button>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    </>
  );
}
