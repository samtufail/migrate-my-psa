import { DButton, Button, Input, CustomSelect } from 'components';
import { ArrowLeftCircleIcon } from '@heroicons/react/24/outline';
import { Formik, Form } from 'formik';
import { Select } from 'antd';
// import { useState } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { APIKeys, useAppSelector, useUpdateAPIKeysMutation } from 'store';
import { getFirebaseErrorMessage } from 'utils';

export function APIForm() {
  const [updateAPIKeys, { isLoading, error, isError }] = useUpdateAPIKeysMutation();
  const user = useAppSelector((state) => state.user);

  const handleSubmit = async (values: APIKeys) => {
    updateAPIKeys(values)
      .unwrap()
      .then((values) => console.log(values))
      .catch((e) => console.log(e));
  };
  // const [timeZoneValue, setTimeZoneValue] = useState('');

  // const timeZone = [
  //   { value: 'UTC-12', label: 'UTC-12 | Baker Island Time' },
  //   { value: 'UTC-11', label: 'UTC-11 | Niue Time' },
  //   { value: 'UTC-10', label: 'UTC-10 | Hawaii-Aleutian Standard Time' },
  //   { value: 'UTC-9', label: 'UTC-9 | Alaska Standard Time' },
  //   { value: 'UTC-8', label: 'UTC-8 | Pacific Standard Time' },
  //   { value: 'UTC-7', label: 'UTC-7 | Mountain Standard Time' },
  //   { value: 'UTC-6', label: 'UTC-6 | Central Standard Time' },
  //   { value: 'UTC-5', label: 'UTC-5 | Eastern Standard Time' },
  //   { value: 'UTC-4', label: 'UTC-4 | Atlantic Standard Time' },
  //   { value: 'UTC-3', label: 'UTC-3 | Brazil Time' },
  //   { value: 'UTC-2', label: 'UTC-2 | Fernando de Noronha Time' },
  //   { value: 'UTC-1', label: 'UTC-1 | Azores Time' },
  //   { value: 'UTC+0', label: 'UTC+0 | Greenwich Mean Time' },
  //   { value: 'UTC+1', label: 'UTC+1 | Central European Time' },
  //   { value: 'UTC+2', label: 'UTC+2 | Eastern European Time' },
  //   { value: 'UTC+3', label: 'UTC+3 | Moscow Time' },
  //   { value: 'UTC+4', label: 'UTC+4 | Gulf Standard Time' },
  //   { value: 'UTC+5', label: 'UTC+5 | Pakistan Standard Time' },
  //   { value: 'UTC+6', label: 'UTC+6 | Bangladesh Standard Time' },
  //   { value: 'UTC+7', label: 'UTC+7 | Indochina Time' },
  //   { value: 'UTC+8', label: 'UTC+8 | China Standard Time' },
  //   { value: 'UTC+9', label: 'UTC+9 | Japan Standard Time' },
  //   { value: 'UTC+10', label: 'UTC+10 | Australian Eastern Time' },
  //   { value: 'UTC+11', label: 'UTC+11 | Norfolk Island Time' },
  //   { value: 'UTC+12', label: 'UTC+12 | New Zealand Standard Time' },
  //   { value: 'UTC+13', label: 'UTC+13 | Tonga Time' },
  //   { value: 'UTC+14', label: 'UTC+14 | Line Islands Time' },
  // ];

  const router = useRouter();

  return (
    <div className="max-w-5xl mx-auto mt-7">
      <div className="flex justify-between items-center">
        <DButton type="button" className="w-[90px]" onClick={() => router.back()}>
          <ArrowLeftCircleIcon className="block h-6 w-6" aria-hidden="true" />
          <p>Back</p>
        </DButton>
        <h2 className="title text-secondary">Settings</h2>
      </div>

      {/* Form */}
      <div className="shadow-md sm:rounded-lg my-4 p-[22px] font-source-sans-pro flex gap-[24px] w-full min-h-[600px]">
        <div className="md:w-[20%] flex flex-col">
          <Link href="/settings/profile">
            <DButton type="button" className="w-[90px] mb-2">
              Profile
            </DButton>
          </Link>
          <Link href="/settings/api">
            <DButton type="button" className="w-[90px] mb-2  bg-secondary">
              API
            </DButton>
          </Link>
          <Link href="/settings/security">
            <DButton type="button" className="w-[90px] ">
              Security
            </DButton>
          </Link>
        </div>
        <div className="md:w-[100%]">
          <h2 className="title text-secondary">API</h2>

          <Formik
            initialValues={{
              ['syncro-apikey']: user?.['syncro-apikey'],
              ['syncro-subdomain']: user?.['syncro-subdomain'],
              ['superops-apikey']: user?.['superops-apikey'],
              ['superops-subdomain']: user?.['superops-subdomain'],
            }}
            enableReinitialize
            onSubmit={(values: APIKeys) => {
              handleSubmit(values);
            }}
          >
            {({ errors, touched }) => (
              <Form className="flex flex-col gap-[25px] w-full">
                <div className='grid grid-cols-2 items-top gap-2'>
                <Input name="syncro-apikey" type="textarea" label="SyncroMSP API Key" placeholder="xxxxx-xxxxx-xxxxxx" />
                <Input name="syncro-subdomain" type="text" label="SyncroMSP Subdomain (Name Only i.e. hexis)" placeholder="your-subdomain" />
                </div>
                <div className='grid grid-cols-2 items-top gap-2'>
                <Input name="superops-apikey" type="textarea" label="SuperOps.Ai API Key" placeholder="xxxxx-xxxxx-xxxxxx" />
                <Input name="superops-subdomain" type="text" label="SuperOps.Ai Subdomain (Name Only i.e. hexis)" placeholder="your-subdomain" />
                </div>
                {/* <CustomSelect name="timeZone" label="Time Zone" placeholder="UTC" onChange={(value) => setTimeZoneValue(value)}>
                  {timeZone.map((option) => (
                    <Select.Option key={option.value} value={option.value}>
                      {option.label}
                    </Select.Option>
                  ))}
                </CustomSelect> */}
                {/* Save button */}
                <Button type="submit" className="flex justify-center items-center">
                  Save Changes
                </Button>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    </div>
  );
}
