import { Formik, Form } from 'formik';
import { Input, Button } from '../components';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { useSignInMutation } from 'store/user/user.api';
import { toast } from 'react-toastify';
import { getFirebaseErrorMessage } from 'utils/fbErrors';

export function Login() {
  const router = useRouter();
  const [signIn] = useSignInMutation();

  const handleSubmit = async (values: { email: string; password: string }) => {
    signIn(values)
      .unwrap()
      .then((values) => router.push('/dashboard'))
      .catch((e) =>
        toast.error(getFirebaseErrorMessage(e?.code), {
          position: 'top-right',
          autoClose: 2500,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: 'light',
        })
      );
  };

  return (
    <>
      <div className=" text-[#64748B] grid md:grid-cols-2 grid-cols-1 min-h-screen">
        {/* Logo */}
        <div className="flex flex-col gap-[36px] items-center justify-center">
          <div className="px-[12px]">
            <h1 className="md:text-[33px] text-[22px] text-center uppercase text-secondary font-norwester">
              Migrate <span className="text-primary">My PSA</span>
            </h1>
          </div>
          <p className="hidden md:flex text-[16px] font-[400]  max-w-[318px] text-center">
            All in one solution to migrate data between SyncroMSP and SuperOps.Ai
          </p>
          <div>
            <img src="/images/left-image.png" alt="left-image" className="hidden md:flex max-w-[349px]" />
          </div>
        </div>

        <div className="flex flex-col gap-[36px] items-center justify-center">
          <h1 className="md:text-[33px] text-[22px] text-center uppercase text-secondary font-norwester">Login</h1>

          <Formik
            initialValues={{ email: '', password: '' }}
            // Add your validation schema here if needed
            onSubmit={handleSubmit}
          >
            {({ errors, touched }) => (
              <Form className=" flex flex-col gap-[30px] w-full max-w-[428px] p-[12px]">
                {/* Input fields */}
                <Input name="email" type="email" label="Email" placeholder="you@example.com" />
                {/* Signup + Forgot password */}
                <div>
                  <Input name="password" type="password" label="Password" placeholder="••••••••••••" />
                  <div className="flex justify-between text-[14px] text-black mt-0">
                    <p>
                      Don't Have an Account?{' '}
                      <Link href="/sign-up">
                        <span className="underline cursor-pointer">Sign Up Now</span>
                      </Link>
                    </p>
                    <Link href="/forgot">
                      <p className="underline cursor-pointer">Forgot Password?</p>
                    </Link>
                  </div>
                </div>
                {/* Sign In button */}
                <Button type="submit" className="flex justify-center items-center">
                  Sign In
                </Button>
                {/* Sign in with Microsoft button */}
                {/* <Button type="button" onClick={() => { }} className="flex gap-[14px] items-center justify-center bg-secondary">
                  <img src="/images/microsoft.png" alt="microsoft" className="w-[20px]" />
                  <div>Sign in with Microsoft</div>
                </Button> */}
              </Form>
            )}
          </Formik>
        </div>
      </div>
    </>
  );
}
