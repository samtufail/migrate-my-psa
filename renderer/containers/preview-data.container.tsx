import { DButton, Button, JSONView } from 'components';
import { ArrowLeftCircleIcon } from '@heroicons/react/24/outline';
import { useRouter } from 'next/router';

export function PreviewData() {
  const router = useRouter();
  return (
    <div className="max-w-5xl mx-auto mt-7">
      {/* Header */}
      <div className="flex justify-between items-center">
        <DButton type="button" className="w-[90px]" onClick={() => router.back()}>
          <ArrowLeftCircleIcon className="block h-6 w-6" aria-hidden="true" />
          <p>Back</p>
        </DButton>
        <h2 className="title text-secondary">Preview Data</h2>
      </div>
      <div className="shadow-md sm:rounded-lg my-4 p-[22px] font-source-sans-pro flex flex-col items-center min-h-[600px]">
        <div className='flex flex-col gap-[25px] w-full max-w-[420px]'>
          <label className="text-secondary font-kollektif">Please verify your data overview and hit start migration to initiate a new migration. If data doesn't seem right, please check your API keys or contact support.</label>
          <div className="h-[300px] w-full shadow-md border border-gray-100 mt-[25px overflow-y-auto">
            <JSONView />
          </div>
          <Button type="button" onClick={() => router.push('/dashboard')} className="flex justify-center items-center">
            Start Migration
          </Button>
        </div>
      </div>
    </div>
  );
}
