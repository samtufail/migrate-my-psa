import { Formik, Form } from 'formik';
import { Input, Button } from '../components';
import { useRouter } from 'next/router';

export function EmailSent() {
  const router = useRouter();
  return (
    <>
      <div className=" text-secondary grid md:grid-cols-2 grid-cols-1 min-h-screen">
        {/* Logo */}
        <div className="flex flex-col gap-[36px] items-center justify-center">
          <div className="px-[12px]">
            <h1 className="md:text-[33px] text-[22px] text-center uppercase text-secondary font-norwester">
              Migrate <span className="text-primary">My PSA</span>
            </h1>
          </div>
          <p className="hidden md:flex text-[16px] font-[400]  max-w-[318px] text-center text-[#64748B]">
            All in one solution to migrate data between SyncroMSP and SuperOps.Ai
          </p>
          <div>
            <img src="/images/left-image.png" alt="left-image" className="hidden md:flex max-w-[349px]" />
          </div>
        </div>

        <div className="flex flex-col gap-[36px] items-center justify-center">
          <h1 className="md:text-[33px] text-[22px] text-center uppercase text-secondary font-norwester">Reset Password</h1>

          <Formik
            initialValues={{ password: '', confirmPass: '' }}
            // Add your validation schema here if needed
            onSubmit={(values) => {
              console.log(values);
            }}
          >
            {({ errors, touched }) => (
              <Form className=" flex flex-col gap-[30px] w-full max-w-[428px] p-[12px]">
                {/* Input fields */}
                <div className="flex flex-col gap-[36px] items-center justify-center">
                  <div className="flex flex-col gap-[18px] w-full items-center max-w-[428px] p-[12px]">
                    <img src="/images/check.svg" alt="check-icon" />
                    <h1 className="md:text-[33px] text-[26px] font-norwester">Email Sent</h1>
                    <p className="max-w-[385px] md:text-[18px] text-[16px] text-center font-source-sans-pro">
                      We&apos;ve sent a password reset link to your email. Please check your inbox (and spam folder) and follow the
                      instructions.
                    </p>
                    <Button type="button" onClick={() => router.push('/home')}>Go Back to Login</Button>
                  </div>
                </div>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    </>
  );
}
