import { DButton, Button, Input } from 'components';
import { ArrowLeftCircleIcon } from '@heroicons/react/24/outline';
import { Formik, Form } from 'formik';
import { useState } from 'react';
import { Modal, Upload } from 'antd';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';

import ImgCrop from 'antd-img-crop';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useAppSelector, useUpdateUserMutation } from 'store';
import { toast } from 'react-toastify';
import { getFirebaseErrorMessage } from 'utils/fbErrors';
import { beforeUpload, getBase64 } from 'utils';

export function ProfileForm() {
  const user = useAppSelector((state) => state.user);
  const [loading, setLoading] = useState(false);
  const router = useRouter();

  const [updateUser, { isLoading, error, isError }] = useUpdateUserMutation();

  const handleSubmit = async (values: { name: string; image: string }) => {
    updateUser(values)
      .unwrap()
      .then((values) => {})
      .catch((e) =>
        toast.error(getFirebaseErrorMessage(e?.code), {
          position: 'top-right',
          autoClose: 2500,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: 'light',
        })
      );
  };

  const uploadButton = (
    <button style={{ border: 0, background: 'none' }} type="button">
      {loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div style={{ marginTop: 8 }}>Upload</div>
    </button>
  );

  return (
    <div className="max-w-5xl mx-auto mt-7">
      <div className="flex justify-between items-center">
        <DButton type="button" className="w-[90px]" onClick={() => router.back()}>
          <ArrowLeftCircleIcon className="block h-6 w-6" aria-hidden="true" />
          <p>Back</p>
        </DButton>
        <h2 className="title text-secondary">Settings</h2>
      </div>

      <div className="shadow-md sm:rounded-lg my-4 p-[22px] font-source-sans-pro flex gap-[20px] w-full">
        <div className="md:w-[20%] flex flex-col">
          <Link href="/settings/profile">
            <DButton type="button" className="w-[90px] mb-2 bg-secondary">
              Profile
            </DButton>
          </Link>
          <Link href="/settings/api">
            <DButton type="button" className="w-[90px] mb-2">
              API
            </DButton>
          </Link>
          <Link href="/settings/security">
            <DButton type="button" className="w-[90px]">
              Security
            </DButton>
          </Link>
        </div>
        <div className="md:w-[60%]">
          <h2 className="title text-secondary">Profile</h2>
          <Formik
            initialValues={{ name: user.displayName, email: user.email, image: user.photoUrl }}
            enableReinitialize
            onSubmit={handleSubmit}
          >
            {({ values, setFieldValue, errors, touched }) => (
              <Form className="flex flex-col gap-[25px] w-full max-w-[420px]">
                {/* Other inputs */}
                <Input name="name" type="text" label="Name" placeholder="Enter your name" />
                <Input name="email" type="email" label="Email" placeholder={user?.email} readOnly />
                {/* Upload Image */}
                <div className="relative border border-[#d9d9d9] p-3 rounded-md flex item-center text-black">
                  <ImgCrop>
                    <Upload
                      name="avatar"
                      listType="picture-card"
                      className="avatar-uploader"
                      showUploadList={false}
                      beforeUpload={beforeUpload}
                      onChange={async (info) => {
                        if (info.file.status === 'uploading') {
                          setLoading(true);
                          return;
                        }
                        if (info.file.status === 'done') {
                          // Get this url from response in real world.
                          const res = await getBase64(info.file.originFileObj as File);
                          setFieldValue('image', res);
                        }
                      }}
                    >
                      {values?.image ? <img src={values?.image} alt="avatar" style={{ width: '100%' }} /> : uploadButton}
                    </Upload>
                  </ImgCrop>
                </div>

                <Button type="submit" className="flex justify-center items-center">
                  Save Changes
                </Button>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    </div>
  );
}
