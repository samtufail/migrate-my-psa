import { DButton, Button, CustomSelect } from 'components';
import { ArrowLeftCircleIcon } from '@heroicons/react/24/outline';
import { Formik, Form, Field } from 'formik';
import { useState } from 'react';
import { Select, Checkbox, DatePicker } from 'antd';
import { useRouter } from 'next/router';

export function MigrationForm() {
  const migrate = [
    { value: 'SyncroMSP', label: 'SyncroMSP' },
    { value: 'SuperOps.Ai', label: 'SuperOps.Ai' },
  ];

  const migrateCheck = ['Tickets', 'Quotes', 'Invoices', 'Subscriptions', 'Customers', 'Contacts', 'Products/Services', 'Scripts', 'Users'];

  const router = useRouter();

  return (
    <div className="max-w-5xl mx-auto mt-7">
      <DButton type="button" className="w-[90px]" onClick={() => router.back()}>
        <ArrowLeftCircleIcon className="block h-6 w-6" aria-hidden="true" />
        <p>Back</p>
      </DButton>

      {/* Form */}
      <div className="relative shadow-md sm:rounded-lg my-4 p-[22px] font-source-sans-pro flex flex-col items-center justify-center">
        <h2 className="title text-secondary">New Migration</h2>

        <Formik
          initialValues={{ migrateFrom: 'SyncroMSP', migrateTo: 'SuperOps.Ai', checkedValues: [], selectedDate: null }}
          enableReinitialize
          onSubmit={(values) => {
            // const formattedDate = values.selectedDate ? values.selectedDate.format('DD/MM/YYYY') : null;
            // console.log({ ...values, selectedDate: formattedDate });
            console.log(values);
            router.push('/preview-data');
          }}
        >
          {({ errors, touched, setFieldValue }) => (
            <Form className="flex flex-col gap-[30px] w-full max-w-[428px] p-[12px] justify-center">
              <CustomSelect
                name="migrateFrom"
                label="Migrate From"
                placeholder="SyncroMSP"
                open={false}
                // onChange={(value: string) => setFieldValue('migrateFrom', value)}
              >
                {migrate.map((option) => (
                  <Select.Option key={option.value} value={option.value}>
                    {option.label}
                  </Select.Option>
                ))}
              </CustomSelect>
              <CustomSelect
                name="migrateTo"
                label="Migrate To"
                placeholder="SuperOps.Ai"
                open={false}
                // onChange={(value: string) => setMigrateToValue(value)}
              >
                {migrate.map((option) => (
                  <Select.Option key={option.value} value={option.value}>
                    {option.label}
                  </Select.Option>
                ))}
              </CustomSelect>

              {/* <Field name="selectedDate">
                {({ field }) => (
                  <DatePicker
                    {...field}
                    onChange={(value) => {
                      field.onChange({
                        target: {
                          name: 'selectedDate',
                          value,
                        },
                      });
                    }}
                    className="p-4"
                    format="DD/MM/YYYY"
                  />
                )}
              </Field> */}

              <Field name="checkedValues">
                {({ field }) => (
                  <Checkbox.Group
                    {...field}
                    options={migrateCheck}
                    className="flex flex-col gap-2"
                    onChange={(values) => {
                      field.onChange({
                        target: {
                          name: 'checkedValues',
                          value: values,
                        },
                      });
                    }}
                  />
                )}
              </Field>

              {/* New Migration button */}
              <Button type="submit" className="flex justify-center items-center">
                Start New Migration
              </Button>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
}
