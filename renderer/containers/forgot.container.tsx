import { Formik, Form } from 'formik';
import { Input, Button, DButton } from '../components';
import { useRouter } from 'next/router';
import { useForgotPasswordMutation } from 'store';
import { ArrowLeftCircleIcon } from '@heroicons/react/24/outline';
import { toast } from 'react-toastify';
import { getFirebaseErrorMessage } from 'utils/fbErrors';

export function Forgot() {
  const router = useRouter();
  const [forgotPassword] = useForgotPasswordMutation();

  const handleSubmit = async (values: { email: string }) => {
    await forgotPassword(values)
      .unwrap()
      .then((res) => router.push('/email-sent'))
      .catch((e) =>
        toast.error(getFirebaseErrorMessage(e?.code), {
          position: 'top-right',
          autoClose: 2500,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: 'light',
        })
      );
  };
  return (
    <>
      <div className=" text-[#64748B] grid md:grid-cols-2 grid-cols-1 min-h-screen">
        {/* Logo */}
        <div className="flex flex-col gap-[36px] items-center justify-center">
          <div className="px-[12px]">
            <h1 className="md:text-[33px] text-[22px] text-center uppercase text-secondary font-norwester">
              Migrate <span className="text-primary">My PSA</span>
            </h1>
          </div>
          <p className="hidden md:flex text-[16px] font-[400]  max-w-[318px] text-center">
            All in one solution to migrate data between SyncroMSP and SuperOps.Ai
          </p>
          <div>
            <img src="/images/left-image.png" alt="left-image" className="hidden md:flex max-w-[349px]" />
          </div>
        </div>

        <div className="flex flex-col gap-[36px] items-center justify-center">
          <DButton type="button" className="w-[90px]" onClick={() => router.back()}>
            <ArrowLeftCircleIcon className="block h-6 w-6" aria-hidden="true" />
            <p>Back</p>
          </DButton>
          <h1 className="md:text-[33px] text-[22px] text-center uppercase text-secondary font-norwester">Forgot Password?</h1>
          <p className="text-secondary body-text max-w-[340px] text-center">
            If your email exists in our system, we will send you a link to reset your password.
          </p>
          <Formik
            initialValues={{ email: '' }}
            // Add your validation schema here if needed
            onSubmit={handleSubmit}
          >
            {({ errors, touched }) => (
              <Form className=" flex flex-col gap-[30px] w-full max-w-[428px] p-[12px]">
                {/* Input fields */}
                <Input name="email" type="email" label="Email" placeholder="you@example.com" />
                {/* button */}
                <Button type="submit" className="flex justify-center items-center">
                  Reset Password
                </Button>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    </>
  );
}
