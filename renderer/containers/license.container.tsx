import { DButton, Button } from 'components';
import { ArrowLeftCircleIcon } from '@heroicons/react/24/outline';
import { Formik, Form } from 'formik';
import { Input } from 'antd';
import { useRouter } from 'next/router';
import { useValidateMutation } from 'store/license';
import { toast } from 'react-toastify';

const { TextArea } = Input;

export function LicenseForm() {
  const [validate, { isLoading, error, isError }] = useValidateMutation();
  const router = useRouter();

  const handleSubmit = async (values: { key: string }) => {
    validate(values)
      .unwrap()
      .then((values) => router.push('/dashboard'))
      .catch((e) =>
        toast.error('Error in validation', {
          position: 'top-right',
          autoClose: 2500,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: 'light',
        })
      );
  };
  return (
    <div className="max-w-5xl mx-auto mt-7">
      {/* Header */}
      <div className="flex justify-between items-center">
        <DButton type="button" className="w-[90px]" onClick={() => router.back()}>
          <ArrowLeftCircleIcon className="block h-6 w-6" aria-hidden="true" />
          <p>Back</p>
        </DButton>
        <h2 className="title text-secondary">Licensing</h2>
      </div>

      {/* Form */}
      <div className="shadow-md sm:rounded-lg my-4 p-[22px] font-source-sans-pro flex flex-col items-center h-[600px]">
        <Formik
          initialValues={{ key: typeof window !== 'undefined' ? localStorage.getItem('key') || '' : '' }}
          enableReinitialize
          onSubmit={handleSubmit}
        >
          {({ errors, touched, handleChange, values }) => (
            <Form className="flex flex-col gap-[25px] w-full max-w-[420px]">
              <div>
                <label className="text-secondary font-kollektif">Enter License Key</label>
                <TextArea
                  name="key"
                  placeholder="xxxxx-xxxxx-xxxxxx"
                  onChange={handleChange}
                  value={values.key || ''}
                  autoSize={{ minRows: 8, maxRows: 12 }}
                />
              </div>
              {/* Active button */}
              <Button type="submit" className="flex justify-center items-center">
                Active
              </Button>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
}
