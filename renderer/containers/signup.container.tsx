import { Formik, Form } from 'formik';
import { Input, Button } from '../components';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useSignUpMutation } from 'store';
import { toast } from 'react-toastify';
import { getFirebaseErrorMessage } from 'utils/fbErrors';

export function SignUp() {
  const router = useRouter()
  const [signUp, { isLoading, error, isError }] = useSignUpMutation();

  const handleSubmit = async (values: { email: string; password: string }) => {
    signUp(values)
      .unwrap()
      .then((values) => router.push('/dashboard'))
      .catch((e) =>
        toast.error(getFirebaseErrorMessage(e?.code), {
          position: 'top-right',
          autoClose: 2500,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: 'light',
        })
      );
  };

  return (
    <>
      <div className=" text-[#64748B] grid md:grid-cols-2 grid-cols-1 min-h-screen">
        {/* Logo */}
        <div className="flex flex-col gap-[36px] items-center justify-center">
          <div className="px-[12px]">
            <h1 className="md:text-[33px] text-[22px] text-center uppercase text-secondary font-norwester">
              Migrate <span className="text-primary">My PSA</span>
            </h1>
          </div>
          <p className="hidden md:flex text-[16px] font-[400]  max-w-[318px] text-center">
            All in one solution to migrate data between SyncroMSP and SuperOps.Ai
          </p>
          <div>
            <img src="/images/left-image.png" alt="left-image" className="hidden md:flex max-w-[349px]" />
          </div>
        </div>

        <div className="flex flex-col gap-[36px] items-center justify-center">
          <h1 className="md:text-[33px] text-[22px] text-center uppercase text-secondary font-norwester">SignUp</h1>

          <Formik
            initialValues={{ email: '', password: '' }}
            // Add your validation schema here if needed
            onSubmit={handleSubmit}
          >
            {({ errors, touched }) => (
              <Form className=" flex flex-col gap-[30px] w-full max-w-[428px] p-[12px]">
                {/* Input fields */}
                <Input name="email" type="email" label="Email" placeholder="you@example.com" />
                {/* Signin + Forgot password */}
                <div>
                  <Input name="password" type="password" label="Password" placeholder="••••••••••••" />
                  <p className="text-black text-[14px]">
                    Already Have an Account? <Link href="/home"><span className="underline cursor-pointer">Sign In Now</span></Link>
                  </p>
                </div>
                {/* Sign up button */}
                <Button type="submit" className="flex justify-center items-center">
                  Sign Up
                </Button>
                {/* Sign up with Microsoft button */}
                {/* <Button type="button" onClick={() => { }} className="flex gap-[14px] items-center justify-center bg-secondary">
                  <img src="/images/microsoft.png" alt="microsoft" className="w-[20px]" />
                  <div>Sign in with Microsoft</div>
                </Button> */}
              </Form>
            )}
          </Formik>
        </div>
      </div>
    </>
  );
}
