import { Migration } from 'components';
import migrationData from 'data/migration.json';

export const MigrationContent = () => {
  return (
    <tbody className="text-[14px] font-[500]">
      {migrationData.map((migration, index) => (
        <tr key={index}>
          <Migration {...migration} />
        </tr>
      ))}
    </tbody>
  );
};
