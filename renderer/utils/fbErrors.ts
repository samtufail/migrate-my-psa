const USER_NOT_FOUND = 'Requested User Not Found!!';
const EMAIL_ALREADY_EXIST = 'Email Already Exists!!';
const INTERNAL_ERROR = 'An internal error occured, please try again!';
const INVALID_CREDENTIAL = 'Provided credentials are invalid!';
const INVALID_EMAIL_FORMAT = 'Email is not formatted properly! Please check and try again.';
const INVALID_PASSWORD_FORMAT = 'Password is not formatted properly! Please check and try again.';
const TOO_MANY_ATTEMPTS = 'Too many requests to login/signup!! Please try again after some time.';
const DEFAULT_MESSAGE = 'An unexpected error occured, please try again!';

export const getFirebaseErrorMessage = (code: string) => {
  switch (code) {
    case 'auth/user-not-found':
      return USER_NOT_FOUND;
    case 'auth/email-already-exists':
      return EMAIL_ALREADY_EXIST;
    case 'auth/email-already-exists':
      return EMAIL_ALREADY_EXIST;
    case 'auth/internal-error':
      return INTERNAL_ERROR;
    case 'auth/invalid-credential':
      return INVALID_CREDENTIAL;
    case 'auth/invalid-email':
      return INVALID_EMAIL_FORMAT;
    case 'auth/invalid-password':
      return INVALID_PASSWORD_FORMAT;
    case 'auth/too-many-requests':
      return TOO_MANY_ATTEMPTS;
    default:
      return DEFAULT_MESSAGE;
  }
};
