const colors = require('tailwindcss/colors')

module.exports = {
  content: [
    './renderer/pages/**/*.{js,ts,jsx,tsx}',
    './renderer/components/**/*.{js,ts,jsx,tsx}',
    './renderer/containers/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {
      colors: {
        primary: "#ff9f1c",
        secondary: "#2ec4b6",
        apricot: "#ffbf69",
        white: '#fff',
        water: "#cbf3f0",
        black: '#000',
      },
      fontFamily: {
        norwester: ['Norwester', 'sans-serif'],
        kollektif: ['Kollektif', 'sans-serif'],
        ['source-sans-pro']: ['SourceSansPro', 'sans-serif']
      },
    },
  },
  plugins: [],
}
