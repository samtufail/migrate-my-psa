import Link from 'next/link';
import { useRouter } from 'next/router';
import { useState } from 'react';
import { setUser, useAppDispatch, useAppSelector, useSignoutMutation } from 'store';

export const Navbar = () => {
  const dispatch = useAppDispatch();
  const [isDropdownOpen, setIsDropdownOpen] = useState(false);
  const [signout] = useSignoutMutation();

  const rUser = useAppSelector((state) => state.user);

  const toggleDropdown = () => {
    setIsDropdownOpen((prevState) => !prevState);
  };

  // Dummy user data
  const user = {
    name: rUser?.displayName || '',
    email: rUser?.email,
    imageUrl: rUser?.photoUrl || '/images/user.png',
  };

  const router = useRouter();

  const links = [
    { name: 'Dashboard', href: '/dashboard', active: router.pathname === '/dashboard' },
    {
      name: 'Settings',
      href: '/settings/profile',
      active: router.pathname === '/settings/profile' || router.pathname === '/settings/api' || router.pathname === '/settings/security',
    },
    { name: 'License', href: '/license', active: router.pathname === '/license' },
  ];

  return (
    <nav className=" p-4 flex items-center justify-between">
      {/* Logo */}
      <div>
        <Link href="/home">
          <a className="font-norwester text-[34px] text-secondary">
            Migrate <span className="text-apricot"> My PSA</span>
          </a>
        </Link>
      </div>

      {/* Routes */}
      <div className="flex justify-center space-x-4 font-source-sans-pro border rounded-full px-6 py-3 shadow-md">
        {links?.map((link) => {
          return (
            <Link href={link.href}>
              <a className={`text-[#96a0aa] hover:text-[#6C757D] ${link.active ? 'bg-[#F7F7F7]' : ''} rounded-full px-4 py-2`}>
                {link.name}
              </a>
            </Link>
          );
        })}
      </div>

      {/* Logout */}
      <div className="relative">
        <button onClick={toggleDropdown} className="flex items-center focus:outline-none">
          <img src={user.imageUrl} alt="User" className="w-8 h-8 rounded-full" />
        </button>

        {/* Dropdown content */}
        {isDropdownOpen && (
          <div className="absolute z-[999] top-10 right-0 bg-white p-4 w-[270px] shadow-md rounded-md">
            <div className="flex items-center space-x-4">
              <img src={user.imageUrl} alt="User" className="w-12 h-12 rounded-full" />
              <div>
                <p className="text-gray-800 font-semibold">{user.name}</p>
                <p className="text-gray-500">{user.email}</p>
              </div>
            </div>
            <hr className="my-3" />
            <button
              onClick={async () => {
                // Handle logout logic here
                await signout();
                dispatch(
                  setUser({
                    email: '',
                    uid: '',
                    displayName: '',
                    photoUrl: '',
                    ['syncro-apikey']: '',
                    ['syncro-subdomain']: '',
                    ['superops-apikey']: '',
                    ['superops-subdomain']: '',
                  })
                );
                router.push('/home');
              }}
              className="text-secondary hover:text-primary"
            >
              Logout
            </button>
          </div>
        )}
      </div>
    </nav>
  );
};
