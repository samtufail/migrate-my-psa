import { Field } from 'formik';
import { Select } from 'antd';

export const CustomSelect = ({ name, label, onChange, placeholder, customErrorLabel, children, open }: any) => {
  return (
    <Field name={name}>
      {({ field, form: { touched, errors, setFieldValue, values }, meta }: any) => {
        const isError = meta.touched && meta.error;
        return (
          <div className="flex flex-col">
            <label htmlFor={name} className="text-[14px] mb-[4px]  text-secondary">
              {label}
            </label>
            <Select
              onChange={(value: any) => {
                setFieldValue(name, value);
                if (onChange) onChange(value);
              }}
              style={{ height: '56px' }}
              className={`text-[#071538] placeholder:text-[#6B7280]  rounded-[6px] ${isError ? 'border-red-500 border' : ''}`}
              placeholder={placeholder}
              open={open}
              value={values[name]}
            >
              {children}
            </Select>
            {!customErrorLabel?.text && meta.touched && meta.error && <div className="text-red-500">{meta.error}</div>}
            {customErrorLabel?.text && meta.touched && meta.error && (
              <div className="flex w-full justify-between">
                <div className="text-red-500">{meta.error}</div>
                <button type="button" onClick={() => customErrorLabel?.action()}>
                  {customErrorLabel?.text}
                </button>
              </div>
            )}
          </div>
        );
      }}
    </Field>
  );
};
