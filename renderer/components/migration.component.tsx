export const Migration = ({ UID, migrationType, startDate, endDate, status }) => {
  return (
    <>
      <td className="px-6 py-4">{UID}</td>
      <td className="px-6 py-4">{migrationType}</td>
      <td className="px-6 py-4">{startDate}</td>
      <td className="px-6 py-4">{endDate}</td>
      <td className="px-6 py-4">{status}</td>
    </>
  );
};
