import { MouseEventHandler, ReactNode } from 'react';

export const DButton = ({
  children,
  type = 'button',
  onClick,
  className = '',
  disabled = false
}: {
  children: ReactNode;
  disabled?: boolean;
  type?: 'submit' | 'button';
  onClick?: MouseEventHandler<HTMLButtonElement> | undefined;
  className?: string;
}) => {
  return (
    <button type={type} className={`p-[10px] w-[160px] flex items-center gap-[8px] bg-[#F9FAFB] h-[37px] rounded-md ${className}`} onClick={onClick}>
      {children}
    </button>
  );
};
