import Head from 'next/head';
import { Navbar } from './navbar.component';

export const Layout = ({ children }) => {
  return (
    <div className="flex flex-col min-h-screen">
      <Head>
        <title>Dashboard</title>
      </Head>
      <Navbar />
      <main className="flex-1">{children}</main>
    </div>
  );
};
